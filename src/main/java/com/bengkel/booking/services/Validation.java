package com.bengkel.booking.services;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Validation {

	public static String validasiInput(String question, String errorMessage, String regex) {
		Scanner input = new Scanner(System.in);
		String result;
		boolean isLooping = true;
		do {
			System.out.print(question);
			result = input.nextLine();

			// validasi menggunakan matches
			if (result.matches(regex)) {
				isLooping = false;
			} else {
				System.out.println(errorMessage);
			}

		} while (isLooping);

		return result;
	}

	public static int validasiNumberWithRange(String question, String errorMessage, String regex, int max, int min) {
		int result;
		boolean isLooping = true;
		do {
			result = Integer.valueOf(validasiInput(question, errorMessage, regex));
			if (result >= min && result <= max) {
				isLooping = false;
			} else {
				System.out.println("Pilihan angka " + min + " s.d " + max);
			}
		} while (isLooping);

		return result;
	}

	public static String validateStringInput() {
        Scanner scanner = new Scanner(System.in);
        String input = "";
        boolean isValid = false;

        do {
            try {
                input = scanner.nextLine();
                isValid = true;
            } catch (Exception e) {
                System.out.println("Input tidak valid. Silakan coba lagi.");
                scanner.nextLine();
            }
        } while (!isValid);

        return input;
    }

	public static int validateIntegerInput() {
        Scanner scanner = new Scanner(System.in);
        int input = 0;
        boolean isValid = false;

        do {
            try {
                input = scanner.nextInt();
                isValid = true;
            } catch (InputMismatchException e) {
                System.out.println("Input harus berupa angka. Silakan coba lagi.");
                scanner.nextLine();
            }
        } while (!isValid);

        return input;
    }

	public static int inputRange(int min, int max) {
        Scanner scanner = new Scanner(System.in);
        int value;
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("Input harus berupa angka.");
                scanner.next();
            }
            value = scanner.nextInt();
            if (value < min || value > max) {
                System.out.println("Input harus di antara " + min + " dan " + max + ".");
            }
        } while (value < min || value > max);
        return value;
    }
}
