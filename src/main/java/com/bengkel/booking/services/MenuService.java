package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	public static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static Scanner input = new Scanner(System.in);
	private static String idCustomer;

	public static void run() {
		String[] listMenu = {"Login", "Exit"};
		int loginChoice = 0;
		boolean isLooping = true;
		
		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			loginChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			System.out.println(loginChoice);
			
			switch (loginChoice) {
			case 1:
				login();
				break;
			default:
				System.out.println("Exit");
				isLooping = false;
				break;
			}
		} while (isLooping);
	}
	
	public static void login() {
		System.out.println("Masukkan Customer Id: ");
		idCustomer =  Validation.validateStringInput();
		System.out.println("Masukkan Password");
		String passCustomer =  Validation.validateStringInput();
		boolean verifikasiLogin = BengkelService.loginApp(idCustomer, passCustomer);
		
		if (verifikasiLogin) {
			mainMenu();
		}
		
	}
	
	public static void mainMenu() {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			System.out.println(menuChoice);
			
			switch (menuChoice) {
			case 1:
				//panggil fitur Informasi Customer
				BengkelService.showCustomerInfo(idCustomer);
				break;
			case 2:
				//panggil fitur Booking Bengkel
				BengkelService.bookService(idCustomer);
				break;
			case 3:
				//panggil fitur Top Up Saldo Coin
				BengkelService.topUpCoin(idCustomer);
				break;
			case 4:
				//panggil fitur Informasi Booking Order
				BengkelService.showBookingInfo(idCustomer);
				break;
			default:
				System.out.println("Logout");
				isLooping = false;
				break;
			}
		} while (isLooping);
		
		
	}
	
	//Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
