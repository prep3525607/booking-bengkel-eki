package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class BengkelService {
	public static List<BookingOrder> bookingOrders = new ArrayList<>();
	private static final int MAX_LOGIN_ATTEMPTS = 3;
	private static int loginAttempts = 0;

	// Silahkan tambahkan fitur-fitur utama aplikasi disini

	public static Customer findCustomerByCustomerId(String customerId) {
		List<Customer> allCustomers = CustomerRepository.getAllCustomer();
		for (Customer customer : allCustomers) {
			if (customer.getCustomerId().equals(customerId)) {
				return customer;
			}
		}
		return null;
	}

	public static ItemService findServicerByServiceId(String serviceId) {
		for (ItemService itemService : ItemServiceRepository.getAllItemService()) {
			if (itemService.getServiceId().equals(serviceId)) {
				return itemService;
			}
		}
		return null;
	}

	// Login
	public static boolean loginApp(String customerId, String password) {
		boolean isLoginSuccess = false;
		Customer customer = findCustomerByCustomerId(customerId);

		if (customer != null) {
			if (customer.getPassword().equals(password)) {
				isLoginSuccess = true;
				loginAttempts = 0;
			} else {
				System.out.println("Password yang anda Masukan Salah!");
				loginAttempts++;
			}
		} else {
			System.out.println("Customer Id Tidak Ditemukan atau Salah!");
			loginAttempts++;
		}

		if (loginAttempts >= MAX_LOGIN_ATTEMPTS) {
			System.out.println("Aplikasi Akan berhenti atau Exit!");
			System.exit(0);
		}

		return isLoginSuccess;
	}

	// Info Customer
	public static void showCustomerInfo(String customerId) {
		Customer customer = findCustomerByCustomerId(customerId);

		if (customer != null) {
			System.out.println("--------------------------");
			System.out.println((customer instanceof MemberCustomer ? "Member" : "Non Member"));
			System.out.println("Customer Profile:");
			System.out.println("--------------------------");
			System.out.println("Customer ID: \t" + customer.getCustomerId());
			System.out.println("Nama: \t\t" + customer.getName());
			System.out.println("Alamat: \t" + customer.getAddress());

			if (customer instanceof MemberCustomer) {
				MemberCustomer memberCustomer = (MemberCustomer) customer;
				System.out.println("Saldo Koin: \t" + memberCustomer.getSaldoCoin());
			}

			System.out.println("Daftar Kendaraan:");
			String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
			String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
			System.out.format(line);
			System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun",
					"Tipe Kendaraan");
			System.out.format(line);
			int number = 1;
			String vehicleType = "";
			for (Vehicle vehicle : customer.getVehicles()) {
				if (vehicle instanceof Car) {
					vehicleType = "Mobil";
				} else {
					vehicleType = "Motor";
				}
				System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(),
						vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
				number++;
			}
			System.out.printf(line);
		} else {
			System.out.println("Customer tidak ditemukan!");
		}
	}

	// Booking atau Reservation
	public static void bookService(String customerId) {
		Customer customer = findCustomerByCustomerId(customerId);

		if (customer != null) {
			System.out.println("Masukkan Vehicle Id:");
			String vehicleId = Validation.validateStringInput();

			// Cek kendaraan pada customer
			Vehicle selectedVehicle = null;
			for (Vehicle vehicle : customer.getVehicles()) {
				if (vehicle.getVehiclesId().equals(vehicleId)) {
					selectedVehicle = vehicle;
					break;
				}
			}

			if (selectedVehicle == null) {
				System.out.println("Kendaraan tidak ditemukan.");
				return;
			}

			// Tampilkan list item service sesuai dengan tipe kendaraan
			List<ItemService> itemServices = new ArrayList<>();
			System.out.println("Daftar Service Tersedia:");
			String formatTable = "| %-2s | %-25s | %-25s | %-15s | %-15s |%n";
			String line = "+----+-------------------------------------------------------------------------------------------+%n";
			System.out.format(line);
			System.out.format(formatTable, "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
			System.out.format(line);
			int number = 1;
			for (ItemService itemService : ItemServiceRepository.getAllItemService()) {
				if (itemService.getVehicleType().equalsIgnoreCase(selectedVehicle.getVehicleType())) {
					System.out.format(formatTable, number, itemService.getServiceId(), itemService.getServiceName(),
							selectedVehicle.getVehicleType(), itemService.getPrice());
					itemServices.add(itemService);
					number++;
				}
			}
			System.out.format(line);

			int maxServiceCount = customer.getMaxNumberOfService();
			System.out.println("Pilih service (masukkan ID):");

			List<ItemService> selectedServices = new ArrayList<>();
			for (int i = 0; i < maxServiceCount; i++) {
				System.out.println("Silahkan masukkan Service Id:");
				String serviceId = Validation.validateStringInput();

				ItemService selectedService = findServicerByServiceId(serviceId);
				if (selectedService == null) {
					System.out.println("Service dengan ID " + serviceId + " tidak ditemukan.");
					continue;
				}

				if ((customer instanceof MemberCustomer) && i < 1) {
					String choice = Validation.validasiInput("Apakah anda ingin menambahkan Service Lainnya? (Y/T)",
							"Masukkan Y/T", "[YTyt]");
					if (choice.equalsIgnoreCase("T")) {
						i++;
					}
				}

				// Tambahkan service ke daftar service yang dipilih
				selectedServices.add(selectedService);
			}

			// Pilih metode pembayaran
			String paymentMethod = "";
			if (customer instanceof MemberCustomer) {
				System.out.println("Pilih metode pembayaran: (1) Saldo Coin (2) Cash");
				int paymentChoice = Validation.inputRange(1, 2);
				if (paymentChoice == 1) {
					paymentMethod = "Saldo Coin";
				} else if (paymentChoice == 2) {
					paymentMethod = "Cash";
				} else {
					System.out.println(
							"Pilihan metode pembayaran tidak valid. Pembayaran akan dilakukan dengan Cash secara default.");
					paymentMethod = "Cash";
				}
			} else {
				System.out.println("Pilihan metode pembayaran: (1) Cash");
				paymentMethod = "Cash";
			}

			// Hitung total biaya service
			double totalCost = 0;
			for (ItemService service : selectedServices) {
				totalCost += service.getPrice();
			}

			if (paymentMethod.equals("Saldo Coin")) {
				totalCost *= 0.9;
			}

			// Proses pembayaran
			System.out.println("Total biaya service: " + totalCost);
			if (paymentMethod.equals("Saldo Coin")) {
				MemberCustomer memberCustomer = (MemberCustomer) customer;
				if (memberCustomer.getSaldoCoin() >= totalCost) {
					memberCustomer.setSaldoCoin(memberCustomer.getSaldoCoin() - totalCost);
					System.out
							.println("Pembayaran berhasil. Saldo Coin Anda sekarang: " + memberCustomer.getSaldoCoin());
				} else {
					System.out.println("Saldo Coin Anda tidak mencukupi. Pembayaran dilakukan dengan Cash.");
				}
			} else {
				System.out.println("Pembayaran berhasil. Terima kasih!");
			}

			// Simpan booking order ke dalam list bookingOrders
			BookingOrder bookingOrder = new BookingOrder();
			bookingOrder.setBookingId(BookingOrder.getUniqueID("Book-") + "-" + customerId);
			bookingOrder.setCustomer(customer);
			bookingOrder.setServices(selectedServices);
			bookingOrder.setPaymentMethod(paymentMethod);
			bookingOrder.setTotalServicePrice(totalCost);
			bookingOrder.calculatePayment();

			bookingOrders.add(bookingOrder);
		} else {
			System.out.println("Customer tidak ditemukan!");
		}
	}

	public static void showBookingInfo(String customerId) {
		Customer customer = findCustomerByCustomerId(customerId);
		System.out.println("Booking Order Menu");
		String formatTable = "| %-2s | %-20s | %-20s | %-17s | %-15s | %-15s | %-30s |%n";
		String line = "+----+--------------------------------------------------------------------------------------------------------------------------------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Method", "Total Sevice",
				"Total Payment", "List Service");
		System.out.format(line);
		int number = 1;
		for (BookingOrder bookingOrder : bookingOrders) {
			if (bookingOrder.getCustomer().equals(customer)) {

				System.out.format(formatTable, number, bookingOrder.getBookingId(),
						bookingOrder.getCustomer().getName(), bookingOrder.getPaymentMethod(),
						bookingOrder.getTotalServicePrice(), bookingOrder.getTotalPayment(),
						printServices(bookingOrder.getServices()));
				number++;

			}
		}
		System.out.format(line);
	}

	public static String printServices(List<ItemService> serviceList) {
		String result = "";
		for (ItemService service : serviceList) {
			result += service.getServiceName() + ", ";
		}
		return result;
	}

	// Top Up Saldo Coin Untuk Member Customer
	public static void topUpCoin(String customerId) {
		Customer customer = findCustomerByCustomerId(customerId);

		if (customer instanceof MemberCustomer) {
			System.out.println("Masukkan besaran Top Up: ");
			double amount = Validation.validateIntegerInput();

			// Memastikan bahwa top up tidak negatif atau nol
			if (amount > 0) {
				MemberCustomer memberCustomer = (MemberCustomer) customer;
				// Menambahkan saldo coin
				double newSaldo = memberCustomer.getSaldoCoin() + amount;
				memberCustomer.setSaldoCoin(newSaldo);
				System.out.println(
						"Top Up Saldo Coin berhasil. Saldo Koin saat ini: " + memberCustomer.getSaldoCoin());

				// Simpan kembali objek MemberCustomer yang sudah diubah ke dalam listAllCustomer
				List<Customer> listAllCustomer = CustomerRepository.getAllCustomer();
				for (int i = 0; i < listAllCustomer.size(); i++) {
					if (listAllCustomer.get(i).getCustomerId().equals(customerId)) {
						listAllCustomer.set(i, memberCustomer);
						CustomerRepository.setAllCustomer(listAllCustomer);
						break;
					}
				}
			} else {
				System.out.println("Jumlah top up harus lebih besar dari 0.");
			}
		} else {
			// Jika customer bukan merupakan MemberCustomer
			System.out.println("Maaf, fitur ini hanya untuk Member saja!");
		}
	}

}
